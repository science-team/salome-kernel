// Copyright (C) 2007-2012  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  SALOME ModuleCatalog : implementation of ModuleCatalog server which parsers xml description of modules
//  File   : PathPrefix.hxx
//  Author : Estelle Deville
//  Module : SALOME
//  $Header: /home/server/cvs/KERNEL/KERNEL_SRC/src/ModuleCatalog/PathPrefix.hxx,v 1.5.2.1.10.2.12.1 2012-04-12 14:05:17 vsr Exp $
//
#ifndef PATH_PREFIX_H
#define PATH_PREFIX_H

#include <string>
#include <vector>

struct PathPrefix
{
  string path ;
  vector<string> listOfComputer ;
} ;

typedef vector<PathPrefix> ListOfPathPrefix ;
        
#endif // PATH_PREFIX_H
