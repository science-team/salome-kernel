:mod:`salome.kernel` -- Package containing the KERNEL python utilities
======================================================================

:mod:`deprecation` -- Indication of deprecated modules and functions
--------------------------------------------------------------------

.. automodule:: salome.kernel.deprecation
   :members:


:mod:`termcolor` -- Display colored text in terminal
----------------------------------------------------

.. automodule:: salome.kernel.termcolor
   :members:
   :exclude-members: TEST_termcolor


:mod:`logger` -- Logging utility
--------------------------------

.. automodule:: salome.kernel.logger

.. autoclass:: Logger
   :members:
   :show-inheritance:

.. autoclass:: ExtLogger
   :members:
   :show-inheritance:

:mod:`enumerate` -- Emulates a C-like enum for python
-----------------------------------------------------

.. automodule:: salome.kernel.enumerate
   :members:

:mod:`uiexception` -- Exception for user error management
---------------------------------------------------------

.. automodule:: salome.kernel.uiexception
   :members:

:mod:`datamodeler` -- Helper for modeling user data
---------------------------------------------------

.. automodule:: salome.kernel.datamodeler
   :members:

.. automodule:: salome.kernel.testdata
   :members:

:mod:`diclookup` -- Smart dictionnary with key/value lookup
-----------------------------------------------------------

.. automodule:: salome.kernel.diclookup
   :members:

:mod:`service` -- Helper for using SALOME kernel services
---------------------------------------------------------

.. automodule:: salome.kernel.services
   :members:

:mod:`studyedit` -- Study editor
--------------------------------

.. automodule:: salome.kernel.studyedit
   :members:

:mod:`unittester` -- Run very basic unit tests
----------------------------------------------

.. automodule:: salome.kernel.unittester
   :members:

:mod:`pyunittester` -- Simple wrapper of the pyunit framework
-------------------------------------------------------------

.. automodule:: salome.kernel.pyunittester
   :members:
