:mod:`salome.kernel.parametric` -- Package for parametric studies
=================================================================

:mod:`study_exchange_vars` -- Handle Exchange Variables
-------------------------------------------------------

.. automodule:: salome.kernel.parametric.study_exchange_vars
   :members:


:mod:`compo_utils` -- Useful functions for SALOME components used in parametric studies
---------------------------------------------------------------------------------------

.. automodule:: salome.kernel.parametric.compo_utils
   :members:


:mod:`pyscript_utils` -- Useful functions for Python scripts used in parametric studies
---------------------------------------------------------------------------------------

.. automodule:: salome.kernel.parametric.pyscript_utils
   :members:
